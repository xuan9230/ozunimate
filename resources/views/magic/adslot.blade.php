<html ng-app="searchApp">
<head>
  <meta charset="UTF-8" />
	<link href="/css/adslot.css" type="text/css" rel="stylesheet">
	<script src = "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.12/angular.js" ></script>
	<script type="text/javascript" src="/js/magic/adslot.js"></script>
  <title>AdSlot Search List</title>
</head>
<body>

	<div class="page-wrap">
	  <header>
			<img src="img/adslot_logo.png"></img>
	  </header>

	  <div ng-controller="SitesCtrl" class="sitesList">
			<form>
	      <input type="text" ng-model="search" placeholder="Search Publishers" id="inputBar"/>
	    </form>
	    <div ng-repeat="site in sites | filter: myFilter as filtered" class="searchList">
				<a href="http://<%site.siteUrl%>" class="siteUrl"><%site.siteUrl%></a>
				<p class="siteDesc"><%site.description%></p>
	    </div>
			<div ng-if="searched && filtered.length==0" class="noResult">We currently don’t have any results for your search, try another.</div>
	  </div>
	</div>

	<footer>
		<img src="img/adslot_logo_black.png"></img>
		<hr/>
		<p>©2012 Adslot | Adslot Publisher | Adslot Create | Terms|Privacy Policy | Payment Policy</p>
	</footer>

</body>
</html>
