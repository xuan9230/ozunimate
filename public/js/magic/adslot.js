var app = angular.module("searchApp", [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });

app.controller('SitesCtrl', function($scope, $http){
  	$scope.sites;
    $scope.categories;
    $scope.searched = false;

    $scope.myFilter = function(site) {
      //default to no match
      var isMatch = false;


      if($scope.search) {
        $scope.searched = true;
        // split input by comma
        var parts = $scope.search.split(',');

        // iterate each of the words that was entered
        parts.forEach(function(part) {
          if(part != "") {

            var regExp = new RegExp(part,"i");
            // check both site name and category description, ignoring case-sensitive
            if(regExp.test(site.siteName) || regExp.test(site.categoryDesc)) {
              isMatch = true;
            }
          }
        });
      }

      return isMatch;
    };

    // initializing: add category description to the site json objects as a new attribute
    $scope.getCategoryDesc = function() {
      if($scope.sites) {
        for(var site in $scope.sites) {
          var categoryDesc = "";
          for(var id in $scope.sites[site].categoryIds) {
            // traverse the category array to get related terms of a given site
            for(var i=0;i<$scope.categories.length;i++) {
              if($scope.sites[site].categoryIds[id] == $scope.categories[i].id) {
                categoryDesc += ' ';
                categoryDesc += $scope.categories[i].description;
              }
            }
          }
          // put the category description as a new attribute in json objects
          $scope.sites[site].categoryDesc = categoryDesc;
        }
      }
    };

    // get the sample data using RESTful API, and then initialize
    // get sites
    $http.get("/api/adslot/sites_json").success(function (response) {
      $scope.sites = response.sites;
      // then gets catogories
      $http.get("/api/adslot/categories_json").success(function (response) {
        $scope.categories = response.categories;
        // after successfully getting them, initialize
        $scope.getCategoryDesc();});
    });

	});
